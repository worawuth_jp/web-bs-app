@extends('layouts.master')

@section('title')
    business app
@stop

@section('body')
    <div class="col-md-12 mt-4">
        <div style="height: 180px">
            <h1 style="color: #D70C65">PROGRAMMERINDY.COM เราคือผู้ให้บริการออกแบบเว็บไซต์</h1>
            <p style="color: #6D6D6D">อาทิ เว็บไซต์ร้านค้าออนไลน์ เว็บไซต์ประกาศซื้อ-ขาย เว็บไซต์ประกาศอสังหาริมทรัพย์ เว็บไซต์จองห้องอาหาร และระบบลููกค้าสัมพันธ์ ระบบลางาน ระบบยืม-คืนเครื่องมือเป็นต้น</p>

            <div class="col-md-12 text-center">
                <a href="{{url('portfolio')}}"><button class="btn btn-outline-dark mb-2 ml-auto mr-auto">คลิกดูผลงาน</button></a>
            </div>
        </div>
        <h3>บริการของเรา</h3>
        <div class="col-md-12 p-0">
            <div class="row text-center p-0">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-1.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Web Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบได้บนแพรตฟอร์มเว็บแอปพลิเคชั่น ได้ทั้ง wordpress wix php html bootstrap react nodejs vuejs และอื่นๆอีกมากมาย พร้อมกับติดต่อกับฐานข้อมูล mysql pgsql sql server
                            <button class="btn btn-outline-info col-md-12 mt-2">คลิกดูผลงาน</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-2.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Mobile App Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบแอปพลิเคชั่นบนมือถือ ทั้ง android และ ios ด้วยภาษา java kotlin และ cross platform อย่าง Flutter(Dart) รวมถึง swift และ xcode
                            <button class="btn btn-outline-info col-md-12 mt-2">คลิกดูผลงาน</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-3.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Desktop App Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบแอปพลิเคชั่นบน คอมพิวเตอร์ หรือ pc ทั่วไป พัฒนาเป็น .NET Framework ตัวอย่างโปรแกรม เช่น โปรแกรมจัดการร้านค้า POS เป็นต้น
                            <button class="btn btn-outline-info col-md-12 mt-2">คลิกดูผลงาน</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $('.t1').addClass('active');
    </script>
@stop
