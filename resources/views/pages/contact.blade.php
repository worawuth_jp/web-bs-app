@extends('layouts.master')

@section('title')
    business app
@stop

@section('body')
    <div class="col-md-12 mt-4">
        <div style="height: 180px">
            <h1 style="color: #D70C65">ติดต่อเรา</h1>
            <p style="color: #6D6D6D">อาทิ เว็บไซต์ร้านค้าออนไลน์ เว็บไซต์ประกาศซื้อ-ขาย เว็บไซต์ประกาศอสังหาริมทรัพย์ เว็บไซต์จองห้องอาหาร และระบบลููกค้าสัมพันธ์ ระบบลางาน ระบบยืม-คืนเครื่องมือเป็นต้น</p>

        </div>
        <a href="{{url('/quotation')}}"><button class="btn btn-info">ใบเสนอราคา</button></a>
    </div>
@stop

@section('footer')
    <script>
        $('.t3').addClass('active');
    </script>
@stop
