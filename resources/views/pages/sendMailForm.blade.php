@extends('layouts.master')

@section('title')
    ใบเสนอราคา
@stop

@section('body')
    <center>
        <div class="col-md-10">
            <div class="mt-3 mb-3"></div>
            <form class="form-group" action="{{url('sendmail')}}" method="post">
                @csrf
                <div class="row mb-2"><h1 class="ml-auto mr-auto">ใบเสนอราคา</h1></div>
                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        <span style="color: red">*</span>ชื่อผู้ส่ง :
                    </div>
                    <div class="col-md-9 text-left">
                        <input class="form-control col-md-8 mr-auto" type="text" placeholder="ชื่อผู้ส่ง"
                               name="FROMNAME" autocomplete="off">
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        <span style="color: red">*</span>Email :
                    </div>
                    <div class="col-md-9  text-left">
                        <input class="form-control col-md-8 mr-auto" placeholder="you@example.com" autocomplete="off"
                               type="email" name="FROM">
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        <span style="color: red">*</span>หัวข้อ :
                    </div>
                    <div class="col-md-9  text-left">
                        <input class="form-control col-md-8 mr-auto" type="text" name="subject" placeholder="ชื่อโปรเจค"
                               autocomplete="off">
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        <span style="color: red">*</span>รายละเอียด :
                    </div>
                    <div class="col-md-9  text-left">
                        <textarea class="form-control col-md-8 mr-auto" name="textt" autocomplete="off"></textarea>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        <span style="color: red">*</span>ราคาที่ต้องการ :
                    </div>
                    <div class="col-md-9 form-inline">
                        <input class="form-control col-md-4 mr-auto" type="text" name="price" placeholder="ไม่มี ใส่ 0">

                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        LINE ID :
                    </div>
                    <div class="col-md-9">
                        <input class="form-control col-md-4 mr-auto" type="text" name="line">
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3 text-right col-form-label">
                        เบอร์ติดต่อ :
                    </div>
                    <div class="col-md-9">
                        <input class="form-control col-md-4 mr-auto" maxlength="10" type="text" name="phone">
                    </div>
                </div>

                <button class="btn btn-info col-md-6">ส่งใบเสนอราคา</button>
            </form>
        </div>
    </center>

@stop

@section('footer')
    <script>
        $('.t3').addClass('active');
    </script>
    @if(count($errors) > 0)
        <script>
            swal("Warning", "กรุณากรอกข้อมูลที่จำเป็นให้ครบถ้วน", "warning");
        </script>
    @endif

    @if(session()->has('status'))
        @if(session()->get('status') == 'success')
            <script>
                swal("Success", "ทำรายการสำเร็จ", "success");
            </script>
        @elseif (session()->get('status') == 'error')
            <script>
                swal("ERROR", "ผิดพลาด! ทำรายการไม่สำเร็จ", "error");
            </script>
        @endif
    @endif
@stop
