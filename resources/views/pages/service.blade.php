@extends('layouts.master')

@section('title')
    business app
@stop

@section('body')
    <div class="col-md-12 mt-4">
        <h1 style="color: #D70C65">บริการของเรา</h1>
        <div class="col-md-12 p-0">
            <div class="row text-center p-0">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-1.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Web Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบได้บนแพรตฟอร์มเว็บแอปพลิเคชั่น ได้ทั้ง wordpress wix php html bootstrap react nodejs vuejs และอื่นๆอีกมากมาย พร้อมกับติดต่อกับฐานข้อมูล mysql pgsql sql server
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-2.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Mobile App Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบแอปพลิเคชั่นบนมือถือ ทั้ง android และ ios ด้วยภาษา java kotlin และ cross platform อย่าง Flutter(Dart) รวมถึง swift และ xcode
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header bg-info">
                            <div class="col-md-12 text-center">
                                <img width="75" height="75" src="{{asset('images/icon-3.png')}}" alt="#">
                            </div>
                            <div class="col-md-12 text-center" style="font-size: 20px">
                                Web Development
                            </div>
                        </div>
                        <div class="card-body text-left" style="max-height: 300px;min-height: 200px">
                            พัฒนาระบบแอปพลิเคชั่นบน คอมพิวเตอร์ หรือ pc ทั่วไป พัฒนาเป็น .NET Framework ตัวอย่างโปรแกรม เช่น โปรแกรมจัดการร้านค้า POS เป็นต้น
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $('.t2').addClass('active');
    </script>
@stop
