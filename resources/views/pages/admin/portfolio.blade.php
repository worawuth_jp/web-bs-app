@extends('layouts.adminLayout')

@section('title')
    admin business app
@stop

@section('body')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">จัดการผลงาน</h1>
        </div>
        <div class="row mb-3">
            <a class="btn btn-outline-dark ml-auto mr-2" href="#" data-toggle="modal" data-target="#addPort">
                <span data-feather="plus"></span>
                เพิ่มผลงาน
            </a>
        </div>
        <div style="font-size: 16px">
            <table class="table table-striped text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th width="250"></th>
                    <th>name</th>
                    <th>รายละเอียด(ย่อ)</th>
                    <th>Link</th>
                    <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                <?php $count = 0; ?>
                @foreach($portfolio as $p)
                    <?php $count = $count + 1; ?>
                    <tr>
                        <td>{{$count}}</td>
                        <td class="col-auto"><img class="d-block" src="@if (isset($p->pic))
                            {{asset($p->pic)}}
                            @else
                            {{asset("images/nopic.PNG")}}
                            @endif" style="height: 150px"></td>
                        <td>{{$p->name}}</td>
                        <td>{{$p->detail}}</td>
                        <td><a href="{{$p->link}}">{{$p->link}}</a></td>
                        <td>
                            <button class="btn btn-info" data-toggle="modal" data-target="#more_{{$p->id}}">
                                ดูเพิ่มเติม
                            </button>
                            <button class="btn btn-warning" data-toggle="modal" data-target="#editPort_{{$p->id}}">
                                แก้ไข
                            </button>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#delPort_{{$p->id}}">ลบ
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="addPort" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">เพิ่มผลงาน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{url('/add/portfolio')}}" class="form-group"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row mb-2">
                                <div id="carouselExampleControls" class="carousel ml-auto mr-auto"
                                     data-interval="false">
                                    <div class="carousel-inner" id="result">
                                        <div class="carousel-item active">
                                            <img class="d-block" src="{{asset('images/nopic.PNG')}}" alt="First slide"
                                                 style="height: 250px">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <input type="file" accept="image/*" id="uploadfile" name="uploadfile[]" hidden multiple>
                                <button type="button" id="uploadBtn" name="uploadBtn"
                                        class="btn btn-info ml-auto mr-auto">upload
                                </button>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-3 text-right col-form-label font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>ชื่อผลงาน :
                                </div>
                                <div class="col-md-9">
                                    <input class="form-control" name="name" type="text">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right col-form-label font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>Platform :
                                </div>
                                <div class="col-md-9">
                                    <select name="platform" class="form-control col-md-5">
                                        <option value="Web Platform">Web Platform</option>
                                        <option value="Desktop Platform">Desktop Platform</option>
                                        <option value="Mobile App Platform">Mobile App Platform</option>
                                        <option value="Service Platform">Service Platform</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right col-form-label font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>รายละเอียด(ย่อ) :
                                </div>
                                <div class="col-md-9">
                                    <textarea name="detail" class="form-control" maxlength="100"></textarea>

                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right col-form-label font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>รายละเอียด(ละเอียด) :
                                </div>
                                <div class="col-md-9">
                                    <textarea name="more" rows="4" maxlength="1000" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right col-form-label font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>link :
                                </div>
                                <div class="col-md-9">
                                    <input name="link" class="form-control" type="text">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    @foreach($portfolio as $p)
        <!-- More Modal -->
            <div class="modal fade" id="more_{{$p->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{{$p->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row mb-2">
                                <div id="moreCarousel_{{$p->id}}" class="carousel ml-auto mr-auto"
                                     data-interval="false">
                                    <div class="carousel-inner" id="result">
                                        @php
                                            $pID = $p->id;
                                            $pics = \App\Pic::get($pID);
                                        @endphp

                                        @foreach($pics as $pi)
                                            <div class="carousel-item @if($pi->isMain == 1) active @endif">
                                                <img class="d-block" src="{{asset($pi->path)}}"
                                                     style="height: 250px">
                                            </div>
                                        @endforeach

                                    </div>
                                    <a class="carousel-control-prev" href="#moreCarousel_{{$p->id}}"
                                       role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#moreCarousel_{{$p->id}}"
                                       role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>

                            <div class="row mb-2">
                                <div class="col-md-3 text-right font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>ชื่อผลงาน :
                                </div>
                                <div class="col-md-9 font-weight-normal">
                                    <span style="color: darkblue">{{$p->name}}</span>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>Platform :
                                </div>
                                <div class="col-md-9 font-weight-normal">
                                    <span style="color: #1b1e21">{{$p->platform}}</span>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>รายละเอียด(ย่อ) :
                                </div>
                                <div class="col-md-9">
                                    <span style="color: #1b1e21">{{$p->detail}}</span>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>รายละเอียด(ละเอียด) :
                                </div>
                                <div class="col-md-9">
                                    <span style="color: #1b1e21">{{$p->more}}</span>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-3 text-right font-weight-bold">
                                    <span class="ml-2" style="color: red">*</span>link :
                                </div>
                                <div class="col-md-9">
                                    <a href="{{$p->link}}">{{$p->link}}</a>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete Modal -->
            <div class="modal fade" id="delPort_{{$p->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">คุณต้องการจะลบผลงานนี้หรือไม่?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{url('/del/portfolio')}}" class="form-group"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="row mb-2">
                                    <div class="col-md-12 text-left">
                                        ลบ <span style="color: blue">{{$p->name}}</span> หรือไม่ ?
                                        <input type="hidden" name="pID" value="{{$p->id}}">
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">cancel</button>
                                <button class="btn btn-primary">ยืนยันการลบ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Modal -->
            <div class="modal fade" id="editPort_{{$p->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">เพิ่มผลงาน</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{url('/edit/portfolio')}}" class="form-group"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="row mb-2">
                                    <div id="carouselExampleControls_{{$p->id}}" class="carousel ml-auto mr-auto"
                                         data-interval="false">
                                        <div class="carousel-inner" id="result_{{$p->id}}">

                                            @php
                                                $pID = $p->id;
                                                $pic = \App\Pic::get($pID);
                                            @endphp

                                            @foreach($pic as $pi)
                                                <div class="carousel-item @if ($pi->isMain == 1)
                                                    active
                                                @endif">
                                                    <img class="d-block" src="{{asset($pi->path)}}"
                                                         style="height: 250px">
                                                </div>
                                            @endforeach

                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls_{{$p->id}}"
                                           role="button"
                                           data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls_{{$p->id}}"
                                           role="button"
                                           data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <input type="file" accept="image/*" id="uploadfile_{{$p->id}}" name="uploadfile[]"
                                           hidden multiple>
                                    <button type="button" onclick="clickUploadFile('uploadfile_{{$p->id}}',{{$p->id}})"
                                            name="uploadBtn" class="btn btn-info ml-auto mr-auto">upload
                                    </button>
                                </div>

                                <div class="row mb-2">
                                    <div class="col-md-3 text-right col-form-label font-weight-bold">
                                        <span class="ml-2" style="color: red">*</span>ชื่อผลงาน :
                                    </div>
                                    <div class="col-md-9">
                                        <input class="form-control" name="name" type="text" value="{{$p->name}}">
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3 text-right col-form-label font-weight-bold">
                                        <span class="ml-2" style="color: red">*</span>Platform :
                                    </div>
                                    <div class="col-md-9">
                                        <select name="platform" class="form-control col-md-5">
                                            <option value="Web Platform"
                                                    @if($p->platform == "Web Platform") selected @endif>Web Platform
                                            </option>
                                            <option value="Desktop Platform"
                                                    @if($p->platform == "Desktop Platform") selected @endif >Desktop
                                                Platform
                                            </option>
                                            <option value="Mobile App Platform"
                                                    @if($p->platform == "Mobile App Platform") selected @endif>Mobile
                                                App Platform
                                            </option>
                                            <option value="Service Platform"
                                                    @if($p->platform == "Service Platform") selected @endif>Service
                                                Platform
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3 text-right col-form-label font-weight-bold">
                                        <span class="ml-2" style="color: red">*</span>รายละเอียด(ย่อ) :
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="detail" class="form-control"
                                                  maxlength="100">{{$p->detail}}</textarea>

                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3 text-right col-form-label font-weight-bold">
                                        <span class="ml-2" style="color: red">*</span>รายละเอียด(ละเอียด) :
                                    </div>
                                    <div class="col-md-9">
                                        <textarea name="more" rows="4" maxlength="1000"
                                                  class="form-control">{{$p->more}}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3 text-right col-form-label font-weight-bold">
                                        <span class="ml-2" style="color: red">*</span>link :
                                    </div>
                                    <div class="col-md-9">
                                        <input name="link" class="form-control" type="text" value="{{$p->link}}">
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="pID" value="{{$p->id}}">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary">บันทึก</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </main>
@stop

@section('footer')
    <script>
        function clickUploadFile(id, num) {
            $('#' + id).click();
        }

        $('.carousel').carousel({
            interval: false,
        });

        $('.t3').addClass('active');

        $('#uploadBtn').click(function () {
            $('#uploadfile').click();
        });

        document.getElementById("uploadfile").addEventListener("change", function (event) {
            var files = event.target.files;
            var result = document.getElementById("result");
            var c = 0;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                if (!file.type.match('image'))
                    continue;
                var picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    var div = document.createElement("div");
                    div.classList.add("carousel-item");
                    if (c == 0) {
                        div.classList.add("active");
                        result.innerHTML = "";
                        c++;
                    }
                    div.innerHTML = "<img class=\"d-block\" src='" + picFile.result + "' alt=\"First slide\" style=\"height: 250px\">";

                    result.insertBefore(div, null);
                });
                //Read the image
                picReader.readAsDataURL(file);
            }
        });
    </script>

    @if(count($errors) > 0)
        <script>
            swal("ผิดพลาด", "กรุณากรอกข้อมูลให้ครบถ้วน", "warning");
        </script>
    @endif

    @if(session()->has('status'))
        <script>
            swal("Success", "บันทึกสำเร็จ", "success");
        </script>
    @endif
@stop

