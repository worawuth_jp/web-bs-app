@extends('layouts.master')

@section('title')
    business app
@stop

@section('body')
    <div class="col-md-12 mt-4">
        <h1 style="color: #D70C65">ผลงาน</h1>
        <div class="col-md-12 p-0 row">
            @foreach($portfolio as $p)
                <div class="col-md-3 mb-2">
                    <div class="card text-center border-rad">
                        <div class="card-body">
                            <img class="d-block img-fluid img-thumbnail ml-auto mr-auto" style="height: 150px"
                                 src="@if (isset($p->pic))
                                 {{asset($p->pic)}}
                                 @else
                                 {{asset("images/nopic.PNG")}}
                                 @endif">

                            <h5>{{$p->name}}</h5>

                            <div class="col-md-12 text-muted pt-0 overflow-hidden"
                                 style="max-height: 100px;min-height: 100px">
                                {{$p->detail}}

                            </div>
                            <button class="btn btn-outline-dark border-rad col-md-12">เพิ่มเติม</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop

@section('footer')
    <script>
        $('.t5').addClass('active');
    </script>
@stop
