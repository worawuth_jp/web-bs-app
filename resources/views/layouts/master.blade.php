<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @include('include.head')
    @yield('head')
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body style="background-color: #E9E9E9;font-family: 'Mitr', sans-serif;">
<div class="ml-auto mr-auto" style="width: 80%;">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block ml-auto mr-auto w-100" src="{{asset('images/banner-img.png')}}" alt="First slide">
                <div class="carousel-caption d-none d-md-block" style="background-color: #0b0b0b">
                    <h1 style="color: #FFE369">บริการรับทำเว็บไซต์</h1>
                    <h5 style="color: #FF50F7">WEB DESIGN PROFRESSIONAL</h5>
                    <p>รับทำโปรเจคจบ สอนทำโครงงาน วิเคราะห์ระบบ และวิเคราะห์ฐานข้อมูล เกี่ยวกับโครงงาน</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block ml-auto mr-auto w-100" src="{{asset('images/banner-img.png')}}" alt="Second slide">
                <div class="carousel-caption d-none d-md-block" style="background-color: #0b0b0b">
                    <h1 style="color: #FFE369">บริการรับทำเว็บไซต์</h1>
                    <h5 style="color: #FF50F7">WEB DESIGN PROFRESSIONAL</h5>
                    <p>บริการรับทำเว็บไซต์ ออกแบบเว็บ เว็บขายของ ร้านค้าออนไลน์สำหรับ บริษัท ร้านค้า หรือ องค์กร</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    @include("include.nav")
    @yield('body')
    @include("include.footer")
    @yield('footer')
</div>
<script>
    $('.carousel').carousel({
        interval: 5000
    })
</script>
</body>
</html>
