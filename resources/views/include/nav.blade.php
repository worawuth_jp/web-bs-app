<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{url('/')}}">PD Business</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav " style="margin-left: auto;padding-right: 1.2rem">
                <li class="nav-item t1">
                    <a class="nav-link" aria-current="page" href="{{URL('/')}}">Home</a>
                </li>
                <li class="nav-item t2">
                    <a class="nav-link" href="{{url('service')}}">บริการของเรา</a>
                </li>
                <li class="nav-item t3">
                    <a class="nav-link" href="{{url('contact')}}">ติดต่อเรา</a>
                </li>
                <li class="nav-item t4">
                    <a class="nav-link" href="{{url('about')}}">เกี่ยวกับเรา</a>
                </li>
                <li class="nav-item t5">
                    <a class="nav-link" href="{{url('portfolio')}}">ผลงาน</a>
                </li>
                <li class="nav-item t6">
                    <a class="nav-link" href="{{url('quotation')}}">ใบเสนอราคา</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

