<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow" style="font-size: 16px">
    <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="/" style="font-size: 18px" >Business App</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <input class="form-control form-control-dark w-50" type="text" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{url('logout')}}">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row" style="font-size: 16px;font-family:'K2D Medium'">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link t1" href="{{url('admin')}}">
                            <span data-feather="home"></span>
                            Dashboard <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link t2" href="#">
                            <span data-feather="file"></span>
                            ใบเสนอราคา
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link t3" href="{{url('/admin/portfolio')}}">
                            <span data-feather="monitor"></span>
                            ผลงาน
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link t4" href="#">
                            <span data-feather="bar-chart-2"></span>
                            รายงาน
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
