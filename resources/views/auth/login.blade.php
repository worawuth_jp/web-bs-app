<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    @include('include.head')
</head>
<body>
<div class="container">
    <div class="col-md-12 mt-5">
        <div class="col-md-8 ml-auto mr-auto">
            <div class="card">
                <div class="card-header bg-dark text-white text-center" style="font-size: 25px">
                    <B>ADMIN</B>
                </div>
                <div class="card-body">
                    <form class="form-group" method="post" action="{{url('/chklogin')}}">
                        @csrf
                        <div class="row mb-2">
                            <div class="col-md-4 text-right col-form-label font-weight-bold">
                                username :
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="username" type="text">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4 text-right col-form-label font-weight-bold">
                                password :
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" name="password" type="password">
                            </div>
                        </div>

                        <div class="row mb-2">
                            <input type="submit" name="submit" value="Login" class="mt-2 btn-outline-dark btn col-md-8 ml-auto mr-auto">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('include.footer2')
</body>
</html>
