<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('/');

Route::get('/service', function () {
    return view('pages.service');
})->name('service');

Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/portfolio', 'RouteController@portfolio')->name('portfolio');

Route::get('/admin','AdminController@index')->name('admin');
Route::post('/chklogin','AdminController@chkUser')->name('chklogin');
Route::get('/logout', 'AdminController@logout')->name('logout');
Route::get('/admin/portfolio','AdminController@portfolio')->name('portfolio');
Route::post('/add/portfolio','PortfolioController@add')->name('addPortfolio');
Route::post('/edit/portfolio','PortfolioController@edit')->name('editPortfolio');
Route::post('/del/portfolio','PortfolioController@delete')->name('delPortfolio');

//SendMailController
Route::get('quotation','SendMailController@index')->name('quatation');
Route::post('sendmail','SendMailController@send')->name('sendmail');

// Console command
Route::get('/clear-cache', function() {
    $run = Artisan::call('config:clear');
    $run = Artisan::call('cache:clear');
    $run = Artisan::call('config:cache');
    return 'FINISHED';
});

Route::get('/setup-first', function() {
    $run = Artisan::call('migrate:install');
    $run = Artisan::call('migrate:fresh',['--seed'=>true]);
    return 'FINISHED';
});

Route::get('/setup-refresh', function() {
    $run = Artisan::call('migrate:fresh',[
        '--seed'=>true
    ]);
    return 'FINISHED';
});
//Console command
