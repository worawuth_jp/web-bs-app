<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Portfolio extends Model
{
    protected $table = 'portfolios';

    public static function getID(){
        return DB::table('portfolios')->orderBy('id','desc')->first('id');
    }

    public static function insert($name,$detail,$more,$link,$platform){
        DB::table('portfolios')->insert([
            'name' => $name,
            'detail' => $detail,
            'more' => $more,
            'link' => $link,
            'platform' => $platform,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

    }

    public static function updateRow($name,$detail,$more,$link,$platform,$id){
        DB::table('portfolios')->where('id',$id)->update([
            'name' => $name,
            'detail' => $detail,
            'more' => $more,
            'link' => $link,
            'platform' => $platform,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public static function getAll(){
        return DB::table('portfolios')->get();
    }

    public static function deleteRow($id){
        DB::table('portfolios')->where('id',$id)->delete();
    }
}
