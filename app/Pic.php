<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pic extends Model
{
    protected $table = "pics";

    public static function getCount(){
        return DB::table('pics')->count();
    }

    public static function insert($path,$pID,$isMain=0){
       DB::table('pics')->insert([
            'path' => $path,
            'pID' => $pID,
            'isMain' => $isMain,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public static function updateRow($path,$pID,$isMain=0){
        $result = DB::table('pics')->where('pID',$pID)->get();
        foreach ($result as $row){
            $id = $row->id;
            DB::table('pics')->delete($id);
        }
        DB::table('pics')->update([
            'path' => $path,
            'pID' => $pID,
            'isMain' => $isMain,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public static function getMain($pID){
        return DB::table('pics')->where('pID','=',$pID)->where('isMain',1)->first();
    }
    public static function getAllMain(){
        return DB::table('pics')->where('isMain',1)->get();
    }
    public static function get($pID){
        return DB::table('pics')->where('pID','=',$pID)->get();
    }

    public static function deleteRow($id){
        DB::table('pics')->where('id',$id)->delete();
    }

    public static function deletePortPic($pID){
        DB::table('pics')->where('pID',$pID)->delete();
        DB::table('pics')->where('pID',$pID)->delete();
    }

}
