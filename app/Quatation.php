<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Quatation extends Model
{
    protected $table = 'quotations';

    public static function insert($fromname,$frommail,$title,$detail,$lineID,$phone,$price){
        $now = date('Y-m-d H:i:s');

        DB::table('quotations')->insert([
            'fromname'=>$fromname,
            'frommail'=>$frommail,
            'title'=>$title,
            'detail'=>$detail,
            'lineID'=>$lineID,
            'phone'=>$phone,
            'price'=>$price,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
