<?php

namespace App\Http\Controllers;

use App\Pic;
use App\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{

    public function __construct()
    {
        return route('login');
    }

    public function chkLogin(){
        if(!Session::has('auth')){
            return redirect()->route('login');
        }
    }

    public function index(){
        if(!Session::has('auth')){
            return redirect()->route('login');
        }
        return view('pages.admin.index');
    }

    public function chkUser(Request $request){
        $user = array(
           'username'=> $request->username,
           'password' => $request->password
        );

        if($user['username'] == 'admin' && $user['password']== '1234'){
            Session::put('auth', 'true');
            Session::put('username', 'admin');
            return redirect()->route('admin');
        }
        else{
            return back()->with('errors','login failed');
        }
    }

    public function logout(Request $request){
       $request->session()->flush();

       return redirect()->route('login');
    }

    public function portfolio(Request $request){
        $portfolios = Portfolio::getAll();
        $pics = Pic::getAllMain();
        foreach ($portfolios as $p){
            foreach ($pics as $pi){
                if($pi->pID == $p->id){
                    $p->pic = $pi->path;
                }
            }
        }
        return view('pages.admin.portfolio',['portfolio'=>$portfolios]);
    }
}
