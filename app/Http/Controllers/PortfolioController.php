<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPortRequest;
use App\Pic;
use App\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function add(AddPortRequest $request){
        $name = $request->post('name');
        $platform = $request->post('platform');
        $detail = $request->post('detail');
        $more = $request->post('more');
        $link = $request->post('link');
        //$file = $request->post('uploadfile');

        $target_dir = "images/portfolio/";
        $fs = "portfolio_";
        $num = Pic::getCount() + 1;

        Portfolio::insert($name,$detail,$more,$link,$platform);
        $pID = Portfolio::getID()->id;

        //print_r($_FILES['uploadfile']['name']);
        for($i=0;$i<count($_FILES['uploadfile']['name']);$i = $i+1){
            $filename = $target_dir.$fs.$num.".".pathinfo($_FILES['uploadfile']['name'][$i],PATHINFO_EXTENSION);
            $num = $num+1;
            echo $i."<br>";
            if(move_uploaded_file($_FILES['uploadfile']['tmp_name'][$i],$filename)){
                $isMain = 0;
                echo "Move"."<br>";
                if($i == 0){
                    $isMain = 1;
                }
                Pic::insert($filename,$pID,$isMain);
            }
            else{
                echo "Upload Failed";
            }
        }
        //echo count($_FILES['uploadfile']['tmp_name']);
        return back()->with('status','success');
    }

    public function edit(AddPortRequest $request){
        $name = $request->post('name');
        $platform = $request->post('platform');
        $detail = $request->post('detail');
        $more = $request->post('more');
        $link = $request->post('link');
        $pID = $request->post('pID');
        //$file = $request->post('uploadfile');

        $target_dir = "images/portfolio/";
        $fs = "portfolio_";
        $num = Pic::getCount() + 1;

        Portfolio::updateRow($name,$detail,$more,$link,$platform,$pID);

        //print_r($_FILES['uploadfile']['name']);
        for($i=0;$i<count($_FILES['uploadfile']['name']);$i = $i+1){
            $filename = $target_dir.$fs.$num.".".pathinfo($_FILES['uploadfile']['name'][$i],PATHINFO_EXTENSION);
            $num = $num+1;
            echo $i."<br>";
            if(move_uploaded_file($_FILES['uploadfile']['tmp_name'][$i],$filename)){
                $isMain = 0;
                echo "Move"."<br>";
                if($i == 0){
                    $isMain = 1;
                }
                Pic::insert($filename,$pID,$isMain);
            }
            else{
                echo "Upload Failed";
            }
        }
        //echo count($_FILES['uploadfile']['tmp_name']);
        return back()->with('status','success');
    }

    public function delete(Request $request){
        $pID = $request->post('pID');

        Pic::deletePortPic($pID);
        Portfolio::deleteRow($pID);
        return back()->with('status','success');
    }
}
