<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormSendMailRequest;
use App\Quatation;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class SendMailController extends Controller
{
    public function index(){
        return view('pages.sendMailForm');
    }

    public function send(FormSendMailRequest $request)
    {
        $mail = new PHPMailer(true);
        $mail->CharSet = 'UTF-8';

        $FROM = $request->post('FROM');
        $FROMNAME = $request->post('FROMNAME');
        $TO = "popcorns1udio@gmail.com"; // recipient email
        $TONAME = "MYNAME";
        $textt = $request->post('textt');
        $subject = $request->post('subject');
        $price = $request->post('price');
        $line = $request->post('line');
        $phone = $request->post('phone');

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = 'smtp.gmail.com';                    // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = 'jay.worawuth@gmail.com';                     // SMTP username
            $mail->Password = 'rxtxhldumrvbgqzq';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->From = $FROM;
            $mail->FromName = $FROMNAME;

            $mail->addAddress($TO, $TONAME);     // Add a recipient
            $mail->addReplyTo($FROM, $FROMNAME);


            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $context = "<div style=\"color: black;font-size: 18px\"><b>รายละเอียดงาน</b></div>" . $textt . '<br><br>'."<div style=\"color: blue;font-size: 18px\">เสนอราคา : <span style='color: #155724'><b style='font-size: 18px'>". number_format($price,2) . " บาท</span></div></b><br><div style=\"color: blue;font-size: 18px\">ข้อมูลในการติดต่อกลับ</div><br><b>LINE ID : </b>&nbsp;".$line."<br><b>เบอร์ติดต่อ :</b>&nbsp;".$phone."<br>";
            $mail->Body = $context;

            $mail->send();
            echo 'Message has been sent';
            Quatation::insert($FROMNAME,$FROM,$subject,$textt,$line,$phone,$price);
            return redirect()->route('quatation')->with('status','success');
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return redirect()->route('quatation')->with('status','error');
        }
    }
}
