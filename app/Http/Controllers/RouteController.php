<?php

namespace App\Http\Controllers;

use App\Pic;
use App\Portfolio;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function portfolio(){
        $portfolio = Portfolio::getAll();
        $pics = Pic::getAllMain();
        foreach ($portfolio as $p){
            foreach ($pics as $pi){
                if($pi->pID == $p->id){
                    $p->pic = $pi->path;
                }
            }
        }
        return view('pages.portfolio',['portfolio'=>$portfolio]);
    }
}
