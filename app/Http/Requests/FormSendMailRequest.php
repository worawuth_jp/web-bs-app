<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormSendMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FROM' => 'required',
            'FROMNAME' => 'required',
            'subject' => 'required',
            'textt' => 'required',
            'price' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'FROM.required' => 'กรุณาใส่อีเมลของท่าน',
            'FROMNAME.required' => 'กรุณาใส่ชื่อของท่าน',
            'subject.required' => 'กรุณาใส่หัวข้อ',
            'textt.required' => 'กรุณาใส่รายละเอียด',
            'price.required' => 'กรุณากำหนดราคาที่ท่านอยากได้ ถ้าไม่มี ใส่ 0',
        ];
    }
}
