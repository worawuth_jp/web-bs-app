<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'platform' => 'required',
            'detail' => 'required',
            'more' => 'required',
            'link' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'กรุณาใส่ชื่อผลงาน',
            'platform.required' => 'กรุณาเลือก platform',
            'detail.required' => 'กรุณาใส่รายละเอียดออย่างย่อ',
            'more.required' => 'กรุณาใส่รายละเอียด',
            'link.required' => 'กรุณาใส่ลิงค์ผลงาน',
        ];
    }
}
